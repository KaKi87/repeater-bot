const Discord = require('discord.js');

const config = require('./config.json');

const messageHandler = require('./messageHandler.js');

const Client = new Discord.Client();

Client.login(config.token);

Client.on('ready', () => messageHandler.init(Client.user.id, Client.guilds.get(config.guild), config.messages));

Client.on('message', message => messageHandler.handle(message));

Client.on('error', err => console.error(err));

process.setMaxListeners(0);

process.on('uncaughtException', err => console.error(err.stack));

process.on('unhandledRejection', err => console.error(`Uncaught Promise Rejection: \n${err.stack}`));

let shutdown = () => {
	console.log('Disconnecting');
	Client.destroy()
		.then(() => {
			console.log('Disconnected');
			process.exit(0);
		})
		.catch(err => {
			console.error(err);
			process.exit(1);
		})
};

process.on('SIGINT', () => shutdown());